asyncapi: '2.4.0'
info:
  title: Telemetry API Specification
  version: '1.0.0'
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0


defaultContentType: application/json

channels:
  new_device_onboarded:
    publish:
      summary: Inform about device getting on-boarded on the platform.
      message:
        $ref: '#/components/messages/new_device_onboarded'
  device_status_change:
    publish:
      summary: Inform the server when the device status gets changed.
      message:
        $ref: '#/components/messages/device_status_change'
  device_resource_change:
    publish:
      summary: Inform the server when the configurtion of the onboarded gets changed.
      message:
        $ref: '#/components/messages/device_resource_change'
  new_service:
    publish:
      summary: Inform the server when a new service has entered in the platform.
      message:
        $ref: '#/components/messages/new_service'
  service_status:
    publish:
      summary: Inform the server when the a service has completed.
      message:
        $ref: '#/components/messages/service_status'
  service_call:
    publish:
      summary: Inform the server when a new service has been invoked.
      message:
        $ref: '#/components/messages/service_call'
  service_remove:
    publish:
      summary: Inform the server when the service gets removed from the platform.
      message:
        $ref: '#/components/messages/service_remove'
  ntx_payment:
    publish:
      summary: Inform the server when a payment has been made.
      message:
        $ref: '#/components/messages/ntx_payment'
  heart_beat:
    publish:
      summary: Inform the server the device is active or not.
      message:
        $ref: '#/components/messages/heart_beat'       
components:
  messages:
    new_device_onboarded:
      payload:
        $ref: "#/components/schemas/new_device_onboarded"
    device_status_change:
      payload:
        $ref: "#/components/schemas/device_status_change"
    device_resource_change:
      payload:
        $ref: "#/components/schemas/device_resource_change"
    device_resource_config:
      payload:
        $ref: "#/components/schemas/device_resource_config"    
    new_service:
      payload:
        $ref: "#/components/schemas/new_service"    
    service_status:
      payload:
        $ref: "#/components/schemas/service_status"
    service_call:
      payload:
        $ref: "#/components/schemas/service_call"
    service_remove:
      payload:
        $ref: "#/components/schemas/service_remove"
    ntx_payment:
      payload:
        $ref: "#/components/schemas/ntx_payment"
    heart_beat:
      payload:
        $ref: "#/components/schemas/heart_beat"
  schemas:
    new_device_onboarded:
      type: object
      properties:
        peer_id:
          $ref: "#/components/schemas/peer_id"
        cpu:
          $ref: "#/components/schemas/cpu"
        ram:
          $ref: "#/components/schemas/ram"
        network:
          $ref: "#/components/schemas/network"
        dedicated_time:
          $ref: "#/components/schemas/dedicated_time"
        timestamp:
          $ref: "#/components/schemas/timestamp"
    device_status_change:
      type: object
      properties:
        peer_id:
          $ref: "#/components/schemas/peer_id"
        status:
          $ref: "#/components/schemas/status"
        reason:
          $ref: "#/components/schemas/reason"
        timestamp:
          $ref: "#/components/schemas/timestamp"
    device_resource_change:
      type: object
      properties:
        peer_id:
          $ref: "#/components/schemas/peer_id"
        changed_attribute_and_value:
          $ref: "#/components/schemas/changed_attribute_and_value"
        timestamp:
          $ref: "#/components/schemas/timestamp"
    device_resource_config:
      type: object
      properties:
        peer_id:
          $ref: "#/components/schemas/peer_id"
        changed_attribute_and_value:
          $ref: "#/components/schemas/changed_attribute_and_value"
        timestamp:
          $ref: "#/components/schemas/timestamp"
    new_service:
      type: object
      properties:
        service_id:
          $ref: "#/components/schemas/service_id"
        service_name:
          $ref: "#/components/schemas/service_name"
        service_description:
          $ref: "#/components/schemas/service_description"
        timestamp:
          $ref: "#/components/schemas/timestamp"
    service_status:
      type: object
      properties:
        call_id:
          $ref: "#/components/schemas/call_id"
        service_id:
          $ref: "#/components/schemas/service_id"
        peer_id_of_service_host:
          $ref: "#/components/schemas/peer_id_of_service_host"
        status:
          $ref: "#/components/schemas/status"
        timestamp:
          $ref: "#/components/schemas/timestamp"
    service_call:
      type: object
      properties:
        call_id:
          $ref: "#/components/schemas/call_id"
        service_id:
          $ref: "#/components/schemas/service_id"
        cpu_used:
          $ref: "#/components/schemas/cpu_used"
        memory_used:
          $ref: "#/components/schemas/memory_used"
        peer_id_of_service_host:
          $ref: "#/components/schemas/peer_id_of_service_host"
        timestamp:
          $ref: "#/components/schemas/timestamp"
        network_bw_used:
          $ref: "#/components/schemas/network_bw_used"
        time_taken:
          $ref: "#/components/schemas/time_taken"
        status:
          $ref: "#/components/schemas/status"
        max_ram:
          $ref: "#/components/schemas/max_ram"
        amount_of_ntx:
          $ref: "#/components/schemas/amount_of_ntx"
    service_remove:
      type: object
      properties:
        call_id:
          $ref: "#/components/schemas/call_id"
        peer_id_of_service_host:
          $ref: "#/components/schemas/peer_id_of_service_host"
        service_id:
          $ref: "#/components/schemas/service_id"
        timestamp:
          $ref: "#/components/schemas/timestamp"
    ntx_payment:
      type: object
      properties:
        amount_of_ntx:
          $ref: "#/components/schemas/amount_of_ntx"
        service_id:
          $ref: "#/components/schemas/service_id"
        peer_id:
          $ref: "#/components/schemas/peer_id"
        timestamp:
          $ref: "#/components/schemas/timestamp"
        call_id:
          $ref: "#/components/schemas/call_id"
        success_fail_status:
          $ref: "#/components/schemas/success_fail_status"
    heart_beat:
      type: object
      properties:
        peer_id:
          $ref: "#/components/schemas/peer_id"
    changed_attribute_and_value:
      type: object
      properties:
        cpu:
          $ref: "#/components/schemas/cpu"
        ram:
          $ref: "#/components/schemas/ram"
        network:
          $ref: "#/components/schemas/network"
        dedicated_time:
          $ref: "#/components/schemas/dedicated_time"
    peer_id:
      type: string
      example: "<unipque_peer_id>"
      description: Unique indetifier of the device assigned by the platform.
    cpu:
      type: integer
      example: 1000
      description: CPU made available to the platform in MHz.
    ram:
      type: integer
      example: 1000
      description: RAM made available to the platform in MHz.
    network:
      type: number
      format: float
      example: 100
      description: Netowrk bandwidth made available to the platform in MB/s.
    dedicated_time:
      type: number
      format: float
      example: 8
      description: Time made available to the platform in MHz.
    timestamp:
      type: number
      format: float
      example: 1659615093
      description: Unix time.
    status:
      type: string
      example: down
      description: Status of the operation 
    service_id:
      type: string
      example: "<service_id>"
      description: Service ID
    service_name: 
      type: string
      example: "<service_name>"
      description: Name of the service
    service_description: 
      type: string
      example: "<service_description>"
      description: Description of the service
    call_id: 
      type: string
      example: "call_id"
      description: Unique identifire of the call
    peer_id_of_service_host: 
      type: string
      example: "peer_id_of_service_host"
      description: Peer id of the host device
    cpu_used: 
      type: string
      example: "cpu_used"
      description: CPU capacity used by the operation
    memory_used: 
      type: string
      example: "memory_used"
      description: Memory consumed by the operation
    network_bw_used: 
      type: string
      example: "network_bw_used"
      description: Network bandwidth used by the operation
    time_taken: 
      type: string
      example: "time_taken"
      description: Time take by the operation
    amount_of_ntx: 
      type: number
      format: int
      example: 10
      description: Value of NTX token
    max_ram: 
      type: string
      example: "max_ram"
      description: Max ram 
    success_fail_status: 
      type: string
      example: "success_fail_status"
      description: Status in success or fail
    reason: 
      type: string
      example: "reason"
      description: Reason of operation