# Telemetry API Specification

This repository shows the API Spec for the Telementry API. 

The generated documentation links can be found here:
- Stable (Main Branch) https://nunet.gitlab.io/open-api/telemetry-api-spec/main/
- Beta (Staging Branch) https://nunet.gitlab.io/open-api/telemetry-api-spec/staging/
- Alpha (Develop Branch) https://nunet.gitlab.io/open-api/telemetry-api-spec/develop/
- Upcoming Features as review/<BRANCH_NAME> https://gitlab.com/nunet/open-api/telemetry-api-spec/-/environments